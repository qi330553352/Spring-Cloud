package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient // 开启路由发现
@EnableCircuitBreaker // 开启断路器装配
@EnableFeignClients // 开启feign client
@SpringBootApplication
public class AdminClientApplication {

	public static void main(String[] args) {

		SpringApplication.run(AdminClientApplication.class, args);
	}

}
