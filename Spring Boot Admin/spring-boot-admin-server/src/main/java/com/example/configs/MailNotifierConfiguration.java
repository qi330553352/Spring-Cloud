/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.configs;

//import de.codecentric.boot.admin.server.config.AdminServerNotifierAutoConfiguration;
//import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
//import de.codecentric.boot.admin.server.notify.MailNotifier;
//import org.springframework.boot.SpringBootConfiguration;
//import org.springframework.boot.autoconfigure.AutoConfigureBefore;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.mail.MailSender;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.thymeleaf.TemplateEngine;
//import org.thymeleaf.spring5.SpringTemplateEngine;
//import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
//import org.thymeleaf.templatemode.TemplateMode;
//
//import java.nio.charset.StandardCharsets;

/** 自定义报警
 * https://www.jianshu.com/p/9b9145eec05a
 * 创  建   时  间： 2020/4/29
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
//@SpringBootConfiguration
//@AutoConfigureBefore({AdminServerNotifierAutoConfiguration.NotifierTriggerConfiguration.class, AdminServerNotifierAutoConfiguration.CompositeNotifierConfiguration.class})
//@ConditionalOnBean({MailSender.class})
//public class MailNotifierConfiguration {
//
//    private final ApplicationContext applicationContext;
//
//    public MailNotifierConfiguration(ApplicationContext applicationContext) {
//        this.applicationContext = applicationContext;
//    }
//
//    @Bean
//    @ConditionalOnMissingBean
//    @ConfigurationProperties("spring.boot.admin.notify.mail")
//    public MailNotifier mailNotifier(JavaMailSender mailSender, InstanceRepository repository) {
//        return new MailNotifier(mailSender, repository, this.mailNotifierTemplateEngine());
//    }
//
//    @Bean
//    public TemplateEngine mailNotifierTemplateEngine() {
//        SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
//        resolver.setApplicationContext(this.applicationContext);
//        resolver.setTemplateMode(TemplateMode.HTML);
//        resolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
//        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
//        templateEngine.addTemplateResolver(resolver);
//        return templateEngine;
//    }
//}
