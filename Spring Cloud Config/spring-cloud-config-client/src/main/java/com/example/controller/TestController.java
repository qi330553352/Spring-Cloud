package com.example.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 创 建 时 间: 2019/10/1
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@RefreshScope //controller bean是单例，需局部刷新
@RestController
public class TestController {

    @Value("${config}")
    public String config;

    @GetMapping("/test")
    public String test() {

        return this.config;
    }
}
