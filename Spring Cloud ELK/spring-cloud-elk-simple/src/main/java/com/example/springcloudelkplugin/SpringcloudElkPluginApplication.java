package com.example.springcloudelkplugin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudElkPluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudElkPluginApplication.class, args);
	}

}
