package com.sz.qzxkj.feign;

import com.sz.qzxkj.entity.FastDfs;
import com.sz.qzxkj.hystrix.FastDfsHystrix;
import io.swagger.annotations.Api;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * 创 建 时 间: 2019/4/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@RestController
@Api(value = "FastDFS文件服务器Api")
@Resource(name = "FastDFS")
@RequestMapping("/FastDfsApi")
@FeignClient(name="sipo-files-plugin",fallbackFactory=FastDfsHystrix.class)
public interface FastDfsApi {


    @PostMapping("/uploadFile")
//    @ApiImplicitParam(name = "file", value = "文件对象", required = true)
//    @ApiOperation(value="上传文件", notes="上传文件到FastDFS文件服务器",httpMethod = "POST")
    String uploadFile(@RequestParam("file") MultipartFile file);

    @GetMapping("/findById/{id}")
    FastDfs findById(@PathVariable Integer id);

    @GetMapping("/queryById")
    FastDfs queryById(@RequestParam("id") Integer id);

    @PostMapping(value = "/find",consumes = MediaType.APPLICATION_JSON_VALUE)
    List<FastDfs> find(@RequestBody FastDfs params);
}
