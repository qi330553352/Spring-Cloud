package com.sz.qzxkj.hystrix;

import com.sz.qzxkj.entity.FastDfs;
import com.sz.qzxkj.feign.FastDfsApi;
import feign.hystrix.FallbackFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 创 建 时 间: 2019/4/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
@Component
public class FastDfsHystrix implements FallbackFactory<FastDfsApi> {

    @Override
    public FastDfsApi create(Throwable e) {
        return new FastDfsApi() {
            @Override
            public String uploadFile(MultipartFile file) {
                log.error("调用FastDfs组件出错啦:"+e);

                return null;
            }

            @Override
            public FastDfs findById(Integer id) {
                log.error("调用FastDfs组件出错啦:"+e);
                return null;
            }

            @Override
            public FastDfs queryById(Integer id) {
                return null;
            }

            @Override
            public List<FastDfs> find(FastDfs params) {

                return null;
            }
        };
    }
}
