package com.sz.qzxkj;

import feign.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@EnableEurekaClient
@SpringBootApplication
public class BaseFilesServer {

	@Bean
	public Logger.Level feignLoggerLevel(){

		return Logger.Level.FULL;
	}


	public static void main(String[] args) {
		SpringApplication.run(BaseFilesServer.class, args);
	}

}
