package com.sz.qzxkj.builder;

import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

/**
 * 创 建 时 间: 2019/4/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@NoArgsConstructor
public abstract class BaseFileBuilder {

    private MultipartFile file;

    public BaseFileBuilder(MultipartFile file) {
        this.file = file;
    }
}
