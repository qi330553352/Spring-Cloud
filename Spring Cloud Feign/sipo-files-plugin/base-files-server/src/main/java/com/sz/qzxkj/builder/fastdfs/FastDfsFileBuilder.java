package com.sz.qzxkj.builder.fastdfs;

import com.sz.qzxkj.FastDFSClient;
import com.sz.qzxkj.builder.BaseFileBuilder;
import com.sz.qzxkj.utils.FastDFSException;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.multipart.MultipartFile;

/**
 * 创 建 时 间: 2019/4/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
public class FastDfsFileBuilder extends BaseFileBuilder {


    /** https://www.cnblogs.com/chiangchou/p/fastdfs.html
     http:192.168.147.132/group1/M00/00/00/wKiThFyxWK-ADUp1AABJf-ESxV870.jpeg
     http:192.168.147.132/group1/M00/00/00/wKiThFyzMCqABy0xAAA32LYeQy8628.png
     */
    public FastDfsFileBuilder(MultipartFile file) {
        FastDFSClient client = new FastDFSClient();
        try {
            String str = client.uploadFileWithMultipart(file);
            log.info("str:"+str);
        } catch (FastDFSException e) {
            e.printStackTrace();
        }
    }
}
