package com.sz.qzxkj.service;

import com.sz.qzxkj.builder.fastdfs.FastDfsFileBuilder;
import com.sz.qzxkj.feign.FastDfsApi;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * 创 建 时 间: 2019/4/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Service
public class FastDfsService implements FastDfsApi {


    @Override
    public String uploadFile(MultipartFile file) {
        FastDfsFileBuilder builder = new FastDfsFileBuilder(file);
        return null;
    }
}
