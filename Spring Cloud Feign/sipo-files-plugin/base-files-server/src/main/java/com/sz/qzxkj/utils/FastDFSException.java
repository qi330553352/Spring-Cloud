package com.sz.qzxkj.utils;

import lombok.Data;
import lombok.NoArgsConstructor;

/** FastDFS 上传下载时可能出现的一些异常信息
 * 创 建 时 间: 2019/4/13
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Data
@NoArgsConstructor
public class FastDFSException extends Exception {

    /* 错误码 */
    private String code;
    /* 错误消息 */
    private String message;

    public FastDFSException(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
