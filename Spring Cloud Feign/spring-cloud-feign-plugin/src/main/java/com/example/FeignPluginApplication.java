package com.example;

import feign.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class FeignPluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeignPluginApplication.class, args);
	}

	@Bean
	public Logger.Level feignLogLevel(){

		return Logger.Level.FULL;
	}
}
