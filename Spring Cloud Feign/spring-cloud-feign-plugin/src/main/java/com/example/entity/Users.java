package com.example.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 创 建 时 间: 2019/9/22
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Data
public class Users implements Serializable {

    private Integer id;
    private String name;
    private Integer age;
    private Date createTime;
}
