package com.example.feign;

import com.example.entity.Users;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 创 建 时 间: 2019/9/22
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@RestController
@RequestMapping("/usersApi")
public interface UsersApi {

    @GetMapping("/findById/{id}")
    Users findById(@PathVariable("id") Integer id);

    @GetMapping("/findByName")
    Users findByName(@RequestParam("name") String name);

    @PostMapping("/findBeansByParams")
    List<Users> findBeansByParams(@RequestBody Users bean);
}
