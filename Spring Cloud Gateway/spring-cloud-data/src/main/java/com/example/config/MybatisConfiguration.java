package com.example.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringBootConfiguration;

/**
 * 创 建 时 间: 2019/8/21
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@SpringBootConfiguration
@MapperScan(basePackages = "com.example.mapper")
public class MybatisConfiguration {


}
