package com.example.config;

import com.wz.solrj.api.impl.SolrAPIImpl;
import com.wz.solrj.server.SolrServer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * 创 建 时 间: 2019/9/28
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@SpringBootConfiguration
public class SolrConfiguration {

    @Value("solr.model")
    private String model;
    @Value("solr.url")
    private String url;
    @Value("${solr.initConnectionModule}")
    private String initConnectionModule;
    @Value("${solr.maxPoolSize}")
    private Integer maxPoolSize;
    @Value("${solr.checkExpirePeriod}")
    private Long checkExpirePeriod;

    @Bean
    public SolrAPIImpl solrAPI() {

        return new SolrAPIImpl(solrServer());
    }


    @Bean
    public SolrServer solrServer() {
        SolrServer bean = new SolrServer();
        bean.setSolrModel(model);
        bean.setHost(url);
        bean.setInitConnectionModules(initConnectionModule);
        bean.setMaxPoolSize(maxPoolSize);
        bean.setCheckExpirePeriod(checkExpirePeriod);
        return bean;
    }

}
