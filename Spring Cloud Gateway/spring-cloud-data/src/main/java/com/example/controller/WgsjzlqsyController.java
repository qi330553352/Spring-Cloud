package com.example.controller;

import com.example.entity.Wgsjzlqsy;
import com.example.service.WgsjzlqsyService;
import com.wz.solrj.api.SolrAPI;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 创 建 时 间: 2019/9/22
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
@RestController
@RequestMapping("/wgsjzlqsy")
@Api(value = "swagger2", description = "swagger2", position = 100, protocols = "http")
@Resource(name = "swagger2")
public class WgsjzlqsyController {

    @Autowired
    private SolrAPI solrAPI;
    @Autowired
    private WgsjzlqsyService service;

    @GetMapping("/list")
    @ApiOperation(value="swagger2", notes="swagger2",httpMethod = "GET")
    public List<Wgsjzlqsy> list(){

        return service.findList();
    }

    @GetMapping("/solr")
    @ApiOperation(value="solr", notes="solr",httpMethod = "GET")
    public void solr(){
        SolrQuery solr = new SolrQuery();
        QueryResponse response = solrAPI.query("projectInfo",solr);
        long numFound = response.getResults().getNumFound();
        log.info("numFound:{}",numFound);
    }
}
