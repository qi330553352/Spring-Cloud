package com.example.mapper;

import com.example.entity.Wgsjzlqsy;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 创 建 时 间: 2019/9/22
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Repository
public interface WgsjzlqsyMapper {


    List<Wgsjzlqsy> findList();

    int batchInserts(@Param("beans") List<Wgsjzlqsy> beans);
}
