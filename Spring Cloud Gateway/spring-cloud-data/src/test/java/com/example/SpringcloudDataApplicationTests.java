package com.example;

import com.example.entity.Wgsjzlqsy;
import com.example.mapper.WgsjzlqsyMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringcloudDataApplicationTests {

	@Autowired
	private WgsjzlqsyMapper mapper;

	@Test
	public void contextLoads() {

	}


	@Test
	public void test() {
		List<Wgsjzlqsy> beans = new ArrayList<>();
		for(int i=0;i<10000;i++){
			Wgsjzlqsy bean = new Wgsjzlqsy();
			bean.setFlh(String.valueOf(i));
			bean.setSqggh(String.valueOf(i));
			bean.setSqgghv(String.valueOf(i));
			beans.add(bean);
		}
		long start = System.currentTimeMillis();
		batchOpretion(beans,500);
		long end = System.currentTimeMillis();
		System.out.println("耗时:"+(end-start)/1000.0+"s");
	}

	public void batchOpretion(List<Wgsjzlqsy> beans,Integer num){
		if(beans.isEmpty()) return;
		if(beans.size()>num){
			List<Wgsjzlqsy> datas = beans.subList(0,num);
			beans.removeAll(datas);
			mapper.batchInserts(beans);
			batchOpretion(beans,num);
		}else{
			mapper.batchInserts(beans);
		}
	}
}
