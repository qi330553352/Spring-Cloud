/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * 创  建   时  间： 2020/8/30
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@Service
public class ClientAuthenticationManager implements AuthenticationManager {
    @Autowired
    private ClientUserDetailsService clientUserDetailsService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String username = principal.getUsername();
        String password = principal.getPassword();
        if (StringUtils.isAnyBlank(username, password)) {
            throw new RuntimeException("用户名与密码均不能为空");
        }
        UserDetails userDetails = clientUserDetailsService.loadUserByUsername(username);
        if (!StringUtils.equals(password, userDetails.getPassword())) {
            throw new RuntimeException("密码输入不正确！");
        }
        return new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword());
    }
}
