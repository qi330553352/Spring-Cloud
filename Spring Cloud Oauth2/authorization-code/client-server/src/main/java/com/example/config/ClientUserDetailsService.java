/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.config;

import com.example.dao.ClientUserRepositories;
import com.example.entity.ClientUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * 创  建   时  间： 2020/8/30
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@Service
public class ClientUserDetailsService implements UserDetailsService {
    @Autowired
    private ClientUserRepositories clientUserRepositories;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 数据库查用户对象实现，这里不做权限控制
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ClientUser clientUser = clientUserRepositories.findOneByUsername(username);
        if(Objects.isNull(clientUser)){
            throw new UsernameNotFoundException("用户不存在");
        }
        return clientUser;
    }
}
