/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.config;

import com.example.dao.ClientUserRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

/**
 * SecurityConfiguration
 * <p>
 * 创  建   时  间： 2020/8/30
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@EnableWebSecurity
@SpringBootConfiguration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    @Autowired
    private ClientUserRepositories clientUserRepositories;

    @Autowired
    private ClientUserDetailsService clientUserDetailsService;

    @Autowired
    private DataSource dataSource;

    /**
     * 用来配置拦截保护的请求
     *
     * @param http http
     * @throws Exception Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //@formatter:off
        http.authorizeRequests() // 限定签名成功的请求
                //仅放通登录接口
                .antMatchers(HttpMethod.GET, "/", "/index", "/api/login").permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic();
        //@formatter:on

//        http.csrf().disable()//禁用了 csrf 功能
//                .authorizeRequests()// 限定签名成功的请求
//                .antMatchers("/decision/**","/govern/**").hasAnyRole("USER","ADMIN")//对decision和govern 下的接口 需要 USER 或者 ADMIN 权限
//                .antMatchers("/","/admin/login").permitAll() // / admin/login 不限定
//                .antMatchers("/admin/**").hasRole("ADMIN")//对admin下的接口 需要ADMIN权限
//                .antMatchers("/oauth/**").permitAll()//不拦截 oauth 开放的资源
//                .anyRequest().permitAll()//其他没有限定的请求，允许访问
//                .and().anonymous()// 对于没有配置权限的其他请求允许匿名访问
//                .and().formLogin()// 使用 spring security 默认登录页面
//                .and().httpBasic();// 启用http 基础验证

    }

    /**
     * 配置用户签名服务 主要是user-details 机制
     *
     * @param auth 签名管理器构造器，用于构建用户具体权限控制
     * @throws Exception Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
         auth.userDetailsService(clientUserDetailsService);
//        auth.userDetailsService(userDetailsService)
//                .passwordEncoder(passwordEncoder());

    }

    /**
     * 用来构建 Filter 链
     *
     * @param web web
     * @throws Exception Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
    }

}
