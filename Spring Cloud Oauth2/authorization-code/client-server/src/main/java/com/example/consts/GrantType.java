/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.consts;

/**
 * 创  建   时  间： 2020/8/30
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
public enum GrantType {
    /**
     * 授权码
     */
    AUTHORIZATION_CODE("authorization_code"),
    /**
     * 简化模式
     */
    IMPLICIT("implicit"),
    /**
     * 客户端模式
     */
    CLIENT_CREDENTIALS("client_credentials"),
    /**
     * 用户密码模式
     */
    PASSWORD("password");

    private String code;
    GrantType(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
