/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 创  建   时  间： 2020/8/30
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@Data
@Entity
@Table(name = "sys_permission")
public class Permission {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //权限名称
    @Column(name="name")
    private String name;

    //权限描述
    @Column(name = "descritpion")
    private String descritpion;

    //授权链接
    @Column(name = "url")
    private String url;

    //父节点id
    @Column(name = "pid")
    private int pid;
}
