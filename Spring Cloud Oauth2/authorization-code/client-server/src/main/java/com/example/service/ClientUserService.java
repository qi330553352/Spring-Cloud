/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.service;

import com.example.dao.ClientUserRepositories;
import com.example.entity.ClientUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * 创  建   时  间： 2020/8/30
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@Service
public class ClientUserService {

    @Autowired
    private ClientUserRepositories repositories;

    public void updateClientUser(ClientUser clientUser){
        Optional<ClientUser> userById = repositories.findById(clientUser.getId());
        if(userById.isPresent()){
            repositories.save(clientUser);
        }
    }
}
