/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.utils;

import com.example.entity.ClientUser;
import com.example.service.ClientUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 * 创  建   时  间： 2020/8/30
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@Component
public class SecurityUtils {

    @Autowired
    private ClientUserService clientUserService;

    public UserDetails getCurrentUser(){
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        Object principal = authentication.getPrincipal();
        if(principal instanceof UserDetails){
            return (UserDetails) principal;
        }
        return null;
    }

    /**
     * 更新token
     */
    public void updateToken(String token){
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        Object principal = authentication.getPrincipal();
        if(principal instanceof ClientUser){
            ClientUser clientUser = (ClientUser) principal;
            clientUser.setAccessToken(token);
            clientUserService.updateClientUser(clientUser);
        }
    }

}
