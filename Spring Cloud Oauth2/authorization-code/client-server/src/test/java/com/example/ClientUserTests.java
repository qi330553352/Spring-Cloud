/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example;

import com.example.dao.ClientUserRepositories;
import com.example.entity.ClientUser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;
import  org.springframework.security.web.access.WebInvocationPrivilegeEvaluator;

/**
 * 创  建   时  间： 2020/8/30
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ClientServerApplication.class)
public class ClientUserTests {

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private MockMvc mockMvc;
    @Autowired
    private ClientUserRepositories clientUserRepositories;

    @Before
    public void init(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @Transactional
    public void insertClientUserTest(){
        ClientUser user = new ClientUser();
        user.setUsername("刺客五六七");
        user.setPassword("567");
        user.setAccessToken("test");
        user.setRefreshToken("test");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis() + 100000000L));
        user.setValidateTokenExpire(calendar);
        ClientUser save = clientUserRepositories.save(user);
        Assert.assertNotNull(save);
        Assert.assertEquals(user.getUsername(), save.getUsername());
        Assert.assertEquals(user.getPassword(), save.getPassword());
    }
    @Test
    public void insertClientUserTest2(){
        ClientUser user = new ClientUser();
        user.setUsername("hellxz");
        user.setPassword(passwordEncoder.encode("abc"));
        ClientUser save = clientUserRepositories.save(user);
        Assert.assertNotNull(save);
        Assert.assertEquals(user.getUsername(), save.getUsername());
        Assert.assertEquals(user.getPassword(), save.getPassword());
    }
}
