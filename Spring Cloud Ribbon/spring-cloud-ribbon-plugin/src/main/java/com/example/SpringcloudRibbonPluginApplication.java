package com.example;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@EnableEurekaClient
@SpringBootApplication
public class SpringcloudRibbonPluginApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudRibbonPluginApplication.class, args);
    }

    @Bean
    public IRule ribbonRule() {

        return new RandomRule();
    }
}
