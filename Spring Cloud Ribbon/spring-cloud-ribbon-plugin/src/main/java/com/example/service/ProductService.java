package com.example.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Service;

/**
 * 创 建 时 间: 2019/9/21
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
@Service
public class ProductService {

    @Autowired
    private LoadBalancerClient client;

    public void listProduct() {
        ServiceInstance si = client.choose("springcloud-provider");
        StringBuilder sb = new StringBuilder();
        sb.append("http://");
        sb.append(si.getHost());
        sb.append(":");
        sb.append(si.getPort());
        sb.append("/list");
        log.info(sb.toString());
    }
}
