package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudSleuthPluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudSleuthPluginApplication.class, args);
	}

}
