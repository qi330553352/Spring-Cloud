package com.example.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** 1、服务降级
 * 创 建 时 间: 2019/8/12
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
@Service
public class DegradationService {


    @Autowired
    private LoadBalancerClient client; //ribbon 负载均衡客户端

    /**
                                            降级参数
     参数	                                            作用          	    默认值	            备注
     fallback.enabled	                                fallback是否可用	    TRUE	当执行失败或者请求被拒绝，是否会尝试调用hystrixCommand.getFallback()
     fallback.isolation.semaphore.maxConcurrentRequests	fallback最大并发度	10	    如果并发数达到该设置值，请求会被拒绝和抛出异常并且fallback不会被调用。
     */
    @HystrixCommand(fallbackMethod = "fallback",
        commandProperties = {
            //默认10秒;如果并发数达到该设置值，请求会被拒绝和抛出异常并且fallback不会被调用。
            @HystrixProperty(name= HystrixPropertiesManager.FALLBACK_ISOLATION_SEMAPHORE_MAX_CONCURRENT_REQUESTS, value="15")
    })
    public List list(){
        ServiceInstance si = client.choose("springcloud-ribbon-product");
        StringBuilder sb = new StringBuilder("");
        sb.append("http://");
        sb.append(si.getHost());
        sb.append(":");
        sb.append(si.getPort());
        sb.append("/list");
        log.info("url:"+sb.toString());
        RestTemplate rt = new RestTemplate();
        ParameterizedTypeReference<List> typeRef = new ParameterizedTypeReference<List>(){};
        ResponseEntity<List> resp = rt.exchange(sb.toString(), HttpMethod.GET, null, typeRef);
        return resp.getBody();
    }

    public List fallback(){
        List list=new ArrayList();
        Map<String,Object> map = new HashMap<>();
        map.put("-1","fallback");
        list.add(map);
        return list;
    }
}
