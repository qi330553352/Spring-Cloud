package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableTurbine
@EnableHystrix
@EnableHystrixDashboard
@EnableCircuitBreaker //开启Hystrix服务降级熔断器

@EnableCaching //开启服务请求缓存
@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class SnowslideApplication {

	public static void main(String[] args) {
		SpringApplication.run(SnowslideApplication.class, args);
	}

}
