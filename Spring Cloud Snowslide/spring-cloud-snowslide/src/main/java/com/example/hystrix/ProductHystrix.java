package com.example.hystrix;

import com.example.entity.Product;
import com.example.service.ProductFeignApi;
import feign.hystrix.FallbackFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * 创 建 时 间: 2019/9/24
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
@Component
public class ProductHystrix implements FallbackFactory<ProductFeignApi> {

    @Override
    public ProductFeignApi create(Throwable e) {
        log.error("出错了:"+e);
        return new ProductFeignApi() {
            @Override
            public Product findById(Integer id) {
                log.error("服务降级后的异常记录:"+e);
                return null;
            }
        };
    }
}
