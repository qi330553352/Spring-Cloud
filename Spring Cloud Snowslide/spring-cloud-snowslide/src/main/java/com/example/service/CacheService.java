package com.example.service;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/** 三、服务请求缓存
 * 创 建 时 间: 2019/9/24
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Service
@CacheConfig(cacheNames={"com.example"})
public class CacheService {

    @Cacheable(key="'product' + #id")
    public void get(Integer id){
        System.out.println("=========get========="+id);

    }

    @CacheEvict(key="'product' + #id")
    public void del(Integer id){
        System.out.println("=========del========="+id);
    }
}
