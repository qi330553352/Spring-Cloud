package com.example.service;

import com.example.entity.Product;
import com.example.hystrix.ProductHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/** 九、Feign的雪崩处理：服务降级处理
 * 创 建 时 间: 2019/9/24
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@RestController
//@Api(value = "FastDFS文件服务器Api")
@Resource(name = "ProductFeign")
@RequestMapping("/ProductFeignApi")
@FeignClient(name="springcloud-snowslide",fallbackFactory=ProductHystrix.class)
public interface ProductFeignApi {

    @GetMapping("/findById/{id}")
    Product findById(@PathVariable("id") Integer id);
}
