/**
 * Copying (c) Qixin Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 创  建   时  间： 2020/6/13
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 深圳市七智星科技有限公司
 */
@Data
@ConfigurationProperties(prefix = "gp.redisson")
public class RedissonProperties {
    private String host = "localhost";
    private String password;
    private int port = 6379;
    private int timeout;
    private boolean ssl;
}
