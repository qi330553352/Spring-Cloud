package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamPluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreamPluginApplication.class, args);
	}

}
