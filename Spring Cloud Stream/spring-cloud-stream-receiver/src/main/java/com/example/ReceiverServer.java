package com.example;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

/**
 * 创 建 时 间: 2019/10/1
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Service
@EnableBinding({IReceiverServer.class})
public class ReceiverServer {

    @StreamListener("inputProduct")
    public void onReceiver(byte[] msg) {
        System.out.println("receiver:"+new String(msg));
    }
}
