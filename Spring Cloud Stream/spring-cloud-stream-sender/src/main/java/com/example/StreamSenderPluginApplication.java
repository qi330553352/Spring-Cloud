package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.stream.annotation.EnableBinding;

import javax.xml.ws.BindingType;

@EnableEurekaClient
@SpringBootApplication
@EnableBinding({ISenderServer.class})
public class StreamSenderPluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreamSenderPluginApplication.class, args);
	}

}
