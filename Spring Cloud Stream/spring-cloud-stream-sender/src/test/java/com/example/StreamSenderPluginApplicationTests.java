package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StreamSenderPluginApplicationTests {

	@Autowired
	private ISenderServer sender;

	@Test
	public void contextLoads() {
		String msg = "hello stream ...";
		Message message= MessageBuilder.withPayload(msg.getBytes()).build();
		this.sender.send().send(message);
	}

}
