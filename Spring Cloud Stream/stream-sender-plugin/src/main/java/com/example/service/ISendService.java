package com.example.service;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.SubscribableChannel;

/**
 * 创 建 时 间: 2019/8/14
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public interface ISendService {

    @Output("agan-exchange")
    SubscribableChannel send();
}
