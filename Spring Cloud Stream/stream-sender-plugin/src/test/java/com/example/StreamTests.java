package com.example;

import com.example.service.ISendService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 创 建 时 间: 2019/8/14
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StreamTests {

    @Autowired
    private ISendService send;

    @Test
    public void send() throws InterruptedException {
        String msg="agan...............";
        Message message= MessageBuilder.withPayload(msg.getBytes()).build();
        this.send.send().send(message);
    }
}
