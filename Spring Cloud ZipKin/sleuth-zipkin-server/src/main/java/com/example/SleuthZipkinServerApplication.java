package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import zipkin.server.EnableZipkinServer;

@EnableZipkinServer
@SpringBootApplication
public class SleuthZipkinServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SleuthZipkinServerApplication.class, args);
	}

}
