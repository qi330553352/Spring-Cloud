package com.example;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 创 建 时 间: 2019/10/2
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
@RestController
public class TestController {


    @GetMapping("/test")
    public long test(){
        for(int i=0,len=20;i<len;i++){
            log.info("i:"+i);
        }
        return System.currentTimeMillis();
    }
}
