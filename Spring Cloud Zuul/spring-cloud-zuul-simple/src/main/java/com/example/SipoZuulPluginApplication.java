package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class SipoZuulPluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SipoZuulPluginApplication.class, args);
	}

}
