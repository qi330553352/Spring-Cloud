package com.example.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * 创 建 时 间: 2019/8/12
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
@Component
public class AccessFilter extends ZuulFilter {

    /**
     * 开启过滤器
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * 过滤器的作用，权限校验
     */
    @Override
    public Object run() {
        RequestContext rc=RequestContext.getCurrentContext();
        HttpServletRequest request=rc.getRequest();
        log.info("--------------pre1-------------------");
        String token=request.getParameter("token");
        if(token==null){
            log.warn("token is null............");
            rc.setSendZuulResponse(false);//代表结束请求，不在继续下级传递。
            rc.setResponseStatusCode(401);
            rc.setResponseBody("{\"result\":\"token is null\"}");
            rc.getResponse().setContentType("text/html;charset=utf-8");
        }else{
            // redis 验证
            log.info("token is OK");
        }
        return null;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {

        return 0;
    }

}
