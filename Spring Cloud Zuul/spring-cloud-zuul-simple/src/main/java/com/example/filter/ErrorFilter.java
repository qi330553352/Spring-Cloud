package com.example.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * 创 建 时 间: 2019/8/12
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
@Component
public class ErrorFilter extends ZuulFilter {

    /**
     * 开启过滤器
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * 过滤器的作用，post验证
     */
    @Override
    public Object run() {
        RequestContext rc=RequestContext.getCurrentContext();
        HttpServletRequest request=rc.getRequest();
        log.info("--------------error-------------------");

        return null;
    }

    @Override
    public String filterType() {
        return "error";
    }

    @Override
    public int filterOrder() {

        return 0;
    }
}
