package com.example;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.persistence.EntityManager;

@SpringBootApplication
public class SpringcloudQuerydslPluginApplication {

	@Bean
	public JPAQueryFactory jpaQueryFactory(EntityManager entityManager){

		return new JPAQueryFactory(entityManager);
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudQuerydslPluginApplication.class, args);
	}

}
