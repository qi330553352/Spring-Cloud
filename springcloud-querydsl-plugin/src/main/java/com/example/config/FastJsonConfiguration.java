package com.example.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

/**
 * 创 建 时 间: 2019/8/24
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Configuration
public class FastJsonConfiguration extends WebMvcConfigurationSupport {


    /*
    WriteNullListAsEmpty  ：List字段如果为null,输出为[],而非null
    WriteNullStringAsEmpty ： 字符类型字段如果为null,输出为"",而非null
    DisableCircularReferenceDetect ：消除对同一对象循环引用的问题，默认为false（如果不配置有可能会进入死循环）
    WriteNullBooleanAsFalse：Boolean字段如果为null,输出为false,而非null
    WriteMapNullValue：是否输出值为null的字段,默认为false。
     */
    @Override
    protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        //调用父类的配置
        super.configureMessageConverters(converters);
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        //创建配置类
        FastJsonConfig fastConfig = new FastJsonConfig();
        //修改配置返回内容的过滤
        fastConfig.setSerializerFeatures(
                SerializerFeature.DisableCircularReferenceDetect,
                SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteNullStringAsEmpty
        );
        fastConverter.setFastJsonConfig(fastConfig);
        //将fastjson添加到视图消息转换器列表内
        converters.add(fastConverter);
    }
}
