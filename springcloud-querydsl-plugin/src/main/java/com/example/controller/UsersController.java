package com.example.controller;

import com.example.entity.SystemUsersInfo;
import com.example.service.SystemUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 创 建 时 间: 2019/8/23
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@RestController
public class UsersController {

    @Autowired
    private SystemUsersService usersService;

    @GetMapping("/test")
    public List<SystemUsersInfo> test(){

        return usersService.list();
    }
}
