package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * 创 建 时 间: 2019/8/23
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@NoRepositoryBean
public interface BaseJPA<T> extends JpaRepository<T, Integer>, QuerydslPredicateExecutor<T>{

}
