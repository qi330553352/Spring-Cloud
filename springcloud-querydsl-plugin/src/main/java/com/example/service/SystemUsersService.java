package com.example.service;

import com.example.entity.QSystemUsersInfo;
import com.example.entity.SystemUsersInfo;
import com.example.repository.SystemUsersRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * 创 建 时 间: 2019/8/23
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Service
public class SystemUsersService {


    @Autowired
    private SystemUsersRepository repository;
    @Autowired //实体管理者
    private EntityManager entityManager;
    //JPA查询工厂
    private JPAQueryFactory queryFactory;

    @PostConstruct
    public void initFactory(){
        queryFactory = new JPAQueryFactory(entityManager);
    }

    public List<SystemUsersInfo> list(){
        QSystemUsersInfo q = QSystemUsersInfo.systemUsersInfo;
        return queryFactory.selectFrom(q).orderBy(q.createTime.desc()).fetch();
    }
}
