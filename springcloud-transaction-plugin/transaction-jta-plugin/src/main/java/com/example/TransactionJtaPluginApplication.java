package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@EnableCaching
@SpringBootApplication
@EnableTransactionManagement
public class TransactionJtaPluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionJtaPluginApplication.class, args);
	}
}
