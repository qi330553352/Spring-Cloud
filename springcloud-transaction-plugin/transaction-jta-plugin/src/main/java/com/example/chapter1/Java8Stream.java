package com.example.chapter1;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.io.*;
import java.text.NumberFormat;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 创 建 时 间: 2019/10/20
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class Java8Stream {


    public static void main(String[] args) {
        double d = 23323.3323232323;
        System.out.println(NumberFormat.getInstance().format(d));//23,323.332。返回当前缺省语言环境的【缺省数值】格式
        System.out.println(NumberFormat.getCurrencyInstance().format(d));//￥23,323.33。返回当前缺省语言环境的【货币】格式
        System.out.println(NumberFormat.getNumberInstance().format(d));//23,323.332。返回当前缺省语言环境的【通用数值】格式
        System.out.println(NumberFormat.getIntegerInstance().format(d));//23,323。返回当前缺省语言环境的【整数】格式
        System.out.println(NumberFormat.getPercentInstance().format(d));//2,332,333%。返回当前缺省语言环境的【百分比】格式
    }


}
