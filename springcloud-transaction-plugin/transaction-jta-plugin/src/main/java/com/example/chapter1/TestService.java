package com.example.chapter1;

import com.example.entity.Order;
import com.example.mapper.OrderMapper;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.PostConstruct;

/**
 * 创 建 时 间: 2019/8/21
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Service
public class TestService {

    @Autowired
    private OrderMapper mapper;

    private TestService proxy;
    @Autowired
    private ApplicationContext ac;
    //被@PostConstruct修饰的方法会在服务器加载Servlet的时候运行，并且只会被服务器执行一次。
    // PostConstruct在构造函数之后执行，init（）方法之前执行。PreDestroy（）方法在destroy（）方法知性之后执行
    @PostConstruct
    public void init(){
        proxy = ac.getBean(TestService.class);
    }

    @Transactional
    public void parent(){
        try{
            //this.child();
            //解决方案1：从AOP上下文中获取当前代理对象
            TestService service = (TestService)AopContext.currentProxy();
            service.child();
            //解决方案2：从Spring上下文中获取代理对象
            proxy.child();
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }

        Order order = new Order();
        order.setTitle("测试定单：parent");
        order.setOrderno("parent");
        order.setAmount(100);
        order.setStatus(0);
        mapper.save(order);
        //log.info("保存成功 id:[{}]",order.getId());
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void child(){
        Order order = new Order();
        order.setTitle("测试定单：child");
        order.setOrderno("child");
        order.setAmount(100);
        order.setStatus(0);
        mapper.save(order);
        //log.info("保存成功 id:[{}]",order.getId());
        int a = 1/0;
    }
}
