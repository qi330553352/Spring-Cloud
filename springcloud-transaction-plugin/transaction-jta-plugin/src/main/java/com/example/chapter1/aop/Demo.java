package com.example.chapter1.aop;

/** 被代理的接口
 * 创 建 时 间: 2019/8/22
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public interface Demo {

    void test1();

    void test2();
}
