package com.example.chapter1.aop;


import org.springframework.cglib.proxy.InvocationHandler;
import org.springframework.cglib.proxy.Proxy;

import java.lang.reflect.Method;

/** 代理对象
 * 创 建 时 间: 2019/8/22
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class MyHandler implements InvocationHandler {

    //代理对象
    private Object targer;

    public MyHandler(Object targer) {
        this.targer = targer;
    }

    @Override
    public Object invoke(Object obj, Method method, Object[] objects) throws Throwable {
        System.out.println("==代理对象执行额外业务================");
        //记录日志、权限控制、事务处理 ... ...
        return method.invoke(targer,objects);//代理对象执行被代理对象的方法
    }

    public static void main(String[] args){
        Demo targer = new DemoImpl(); //（真实）被代理对象
        MyHandler handler = new MyHandler(targer); //代理对象
        Demo proxyDemo = (Demo) Proxy.newProxyInstance(MyHandler.class.getClassLoader(),
                new Class[]{Demo.class},handler);//使用反射生成一个被代理的对象
        proxyDemo.test1();
        proxyDemo.test2();
    }
}
