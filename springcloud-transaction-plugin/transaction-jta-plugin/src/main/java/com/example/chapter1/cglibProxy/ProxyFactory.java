package com.example.chapter1.cglibProxy;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/** Cglib子类代理工厂
 * 创 建 时 间: 2019/10/20
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class ProxyFactory implements MethodInterceptor {

    private Object target; // 维护目标对象

    public ProxyFactory(Object target) {
        this.target = target;
    }

    // 给目标对象创建一个代理对象
    public Object getProxyInstance(){
        Enhancer en = new Enhancer(); //1.工具类
        en.setSuperclass(target.getClass());  //2.设置父类
        en.setCallback(this);  //3.设置回调函数
        return en.create();  //4.创建子类(代理对象)
    }
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("向观众问好");
        //执行目标对象的方法
        Object returnValue = method.invoke(target, objects);
        System.out.println("谢谢大家");
        return returnValue;
    }

    public static void main(String[] args) {
        Demo demo = new Demo(); //目标对象
        //代理对象
        Demo proxy = (Demo)new ProxyFactory(demo).getProxyInstance();
        proxy.test1(); //执行代理对象的方法
    }
}
