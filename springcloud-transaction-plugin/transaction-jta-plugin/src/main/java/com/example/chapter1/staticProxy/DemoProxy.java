package com.example.chapter1.staticProxy;

/**
 * 创 建 时 间: 2019/10/20
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class DemoProxy implements Demo{

    private Demo demo;

    public DemoProxy(Demo demo) {
        this.demo = demo;
    }


    @Override
    public void test1() {
        System.out.println("该方法使用静态代理之前执行。。。");
        demo.test1();
        System.out.println("该方法使用静态代理之后执行。。。");
    }


   public static void main(String[] args) {
        Demo demo = new DemoImpl();
        Demo demoProxy = new DemoProxy(demo);
        demoProxy.test1();
   }
}
