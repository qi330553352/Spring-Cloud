package com.example.chapter2.nettyProject;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.springframework.core.codec.StringDecoder;

/**
 * 创 建 时 间: 2019/9/9
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class NettyServer {


    public void start(String hostName, int ip) throws InterruptedException {
        final ServerBootstrap bootstrap = new ServerBootstrap();
        NioEventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        bootstrap.group(eventLoopGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
//                        pipeline.addLast(new NettyMessageDecoder(65536, 4, 4));
//                        pipeline.addLast(new NettyMessageEncoder());
//                        pipeline.addLast(new NettyServer);
                    }
                });
        bootstrap.bind(hostName,ip).sync();

    }
}
