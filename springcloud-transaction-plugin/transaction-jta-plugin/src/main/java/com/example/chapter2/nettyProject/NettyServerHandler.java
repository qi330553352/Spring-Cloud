package com.example.chapter2.nettyProject;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 创 建 时 间: 2019/9/9
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class NettyServerHandler extends ChannelInboundHandlerAdapter {

    private Map<String,Object> transactionTypeMap = new HashMap<>();

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("接收数据:"+msg.toString());
        JSONObject obj = JSON.parseObject((String) msg);
        String command = obj.getString("command");//create-创建事务组 add-添加事务组
        String groupId = obj.getString("groupId");//事务组ID
        String transactionType = obj.getString("transactionType");//子事务类型，commit-待提交，rollback-待回滚
        Boolean isEnd = obj.getBoolean("isEnd");//是否是结束事务
        if("create".equals(command)){
            transactionTypeMap.put(groupId,new ArrayList<String>());
        }


    }
}
