package com.example.chapter2.nettyProject;

/**
 * 创 建 时 间: 2019/9/9
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class TxManagerMain {

    public static void main(String[] args) throws InterruptedException {
        NettyServer nettyServer = new NettyServer();
        nettyServer.start("127.0.0.1",8888);
        System.out.println("Netty 启动成功");
    }

}
