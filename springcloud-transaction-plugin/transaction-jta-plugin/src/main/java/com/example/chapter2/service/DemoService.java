package com.example.chapter2.service;

import com.alibaba.druid.util.HttpClientUtils;
import com.example.chapter2.transaction.annotation.LbTransactional;
import com.example.entity.Order;
import com.example.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 创 建 时 间: 2019/9/9
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Service
public class DemoService {

    @Autowired
    private OrderMapper mapper;

    @Transactional
    @LbTransactional(isStart = true)
    public void test(){
        mapper.save(new Order());
        //HttpClient.get("http://service2/test"); //RPC 调用库存系统 减库存操作
        int a = 1/0;
    }

}
