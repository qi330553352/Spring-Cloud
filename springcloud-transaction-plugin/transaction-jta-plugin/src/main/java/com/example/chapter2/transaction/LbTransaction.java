package com.example.chapter2.transaction;

/**
 * 创 建 时 间: 2019/9/9
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class LbTransaction {

    private String transactionId;
    private String transactionGroupId;
    private LbTransactionType transactionType;

    public LbTransaction(String transactionId, String transactionGroupId, LbTransactionType transactionType) {
        this.transactionId = transactionId;
        this.transactionGroupId = transactionGroupId;
        this.transactionType = transactionType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionGroupId() {
        return transactionGroupId;
    }

    public void setTransactionGroupId(String transactionGroupId) {
        this.transactionGroupId = transactionGroupId;
    }

    public LbTransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(LbTransactionType transactionType) {
        this.transactionType = transactionType;
    }
}
