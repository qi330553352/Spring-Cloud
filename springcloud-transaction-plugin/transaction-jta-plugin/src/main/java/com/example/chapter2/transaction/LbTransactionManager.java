package com.example.chapter2.transaction;

import com.example.chapter2.transaction.annotation.LbTransactional;

import java.util.UUID;

/**
 * 创 建 时 间: 2019/9/9
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class LbTransactionManager {

    //private static NettyClient nettyClient;


    //添加事务组
    public static String createTransactionGroup(){

        return UUID.randomUUID().toString().replaceAll("-","").toUpperCase();
    }

    //创建事务
    public static LbTransaction createTransaction(String groupId){
        String lbTransaction = UUID.randomUUID().toString().replaceAll("-","").toUpperCase();
        return new LbTransaction(groupId,lbTransaction,LbTransactionType.commit);
    }

    //添加事务
    public static void addLbTransaction(LbTransaction tr, boolean isend){

    }
}
