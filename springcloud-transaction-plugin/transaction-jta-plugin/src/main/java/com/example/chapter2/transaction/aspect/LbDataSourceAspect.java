package com.example.chapter2.transaction.aspect;

import com.example.chapter2.transaction.connection.LbConnection;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.sql.Connection;

/**
 * 创 建 时 间: 2019/9/9
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Aspect
public class LbDataSourceAspect {

    @Around("execution(* javax.sql.DataSource.getConnection(..))")
    public Connection around(ProceedingJoinPoint joinPoint){
        try {
            Connection connection = (Connection) joinPoint.proceed();
            return new LbConnection(connection);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }
}
