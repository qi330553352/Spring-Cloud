package com.example.chapter2.transaction.aspect;

import com.example.chapter2.transaction.LbTransaction;
import com.example.chapter2.transaction.LbTransactionManager;
import com.example.chapter2.transaction.LbTransactionType;
import com.example.chapter2.transaction.annotation.LbTransactional;
import com.example.chapter2.transaction.connection.LbConnection;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.Ordered;

import java.lang.reflect.Method;
import java.sql.Connection;

/**
 * 创 建 时 间: 2019/9/9
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Aspect
public class LbTransactionAspect implements Ordered {

    @Around("@annotation(com.example.chapter2.transaction.annotation.LbTransactional)")
    public void invok(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        LbTransactional lbanotation = method.getAnnotation(LbTransactional.class);
        String groupId = "";
        if(lbanotation.isStart()){
            //创建事务组
            groupId = LbTransactionManager.createTransactionGroup();
            //添加本地事务到事务组
        }
        LbTransaction lbTransaction = LbTransactionManager.createTransaction(groupId);
        try {
            joinPoint.proceed(); //spring本身逻辑
            lbTransaction.setTransactionType(LbTransactionType.commit);
        }catch (Throwable e){
            e.printStackTrace();
            lbTransaction.setTransactionType(LbTransactionType.rollback);
        }
        LbTransactionManager.addLbTransaction(lbTransaction,lbanotation.isEnd());
    }

    @Override
    public int getOrder() {
        return 999;
    }
}
