/**
 * Copying (c) Yurian Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * 配置druid使用事务要回滚
 *
 * 创  建   时  间： 2020/4/4
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 悠喃食品(深圳)有限公司
 */
@SpringBootConfiguration
public class DruidTransactionConfiguration {

    @Bean
    public DataSourceTransactionManager transactionManager(DataSource dataSource){

        return new DataSourceTransactionManager(dataSource);
    }
}
