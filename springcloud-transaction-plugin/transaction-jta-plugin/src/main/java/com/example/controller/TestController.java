package com.example.controller;

import com.example.chapter1.TestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 创 建 时 间: 2019/8/22
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@RestController
@Api(value = "用户组件", description = "消息操作 API", position = 100, protocols = "http")
@Resource(name = "对外接口")
@RequestMapping("/UsersApi")
public class TestController {

    @Autowired
    private TestService service;

    @GetMapping("/test")
    @ApiImplicitParam(name = "id", value = "用户文件对象ID", required = true, dataType = "Long")
    @ApiOperation(value="获得用户文件对象", notes="根据id获得用户文件对象",httpMethod = "GET")
    public Long test(long id){
//        service.child();
        return System.currentTimeMillis();
    }
}
