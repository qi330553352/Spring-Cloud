package com.example.mapper;

import com.example.entity.Order;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 创 建 时 间: 2019/8/21
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@CacheNamespace // 开启二级缓存
public interface OrderMapper {


    int save(Order order);
}
