/**
 * Copying (c) Yurian Technoligies Co.,Ltd.2019-2029.all rights reserved.
 */

package com.example.transactionals;

import com.example.entity.Order;
import com.example.mapper.OrderMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * 事务底层解密
 * 【腾讯课堂】：华为老司机最细节的Spring事务实战专题
 *
 * 创  建   时  间： 2020/4/4
 * 版           本: V1.0
 * 作           者: qixin
 * 版  权   所  有: 版权所有(C)2019-2029
 * 公           司: 悠喃食品(深圳)有限公司
 */
@Log4j2
@Service
public class Example1Service {
    @Autowired
    private OrderMapper mapper;

    /**
     * 需求：
     * 1、在执行parent()方法之前要先执行child()额外业务
     * 2、child方法不重要，意味着child方法有异常不能影响parent()方法正常执行
     *
     * 怎么实现?
     * 结果：child()与parent()都执行成功
     */
    @Transactional
    public void parent(){
        log.info("=====parent()方法被调用==========");

        try {
            this.child();
        }catch (ArithmeticException e){
            e.printStackTrace();
            // 手动回滚
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        Order order = new Order();
        order.setTitle("测试定单：parent");
        order.setOrderno("parent");
        order.setAmount(100);
        order.setStatus(0);
        mapper.save(order);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void child(){
        log.info("=====child()方法被调用==========");
        Order order = new Order();
        order.setTitle("测试定单：child");
        order.setOrderno("child");
        order.setAmount(200);
        order.setStatus(1);
        mapper.save(order);
        int a = 1/0;
    }
}
