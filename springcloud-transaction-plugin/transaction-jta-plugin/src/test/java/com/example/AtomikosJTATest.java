package com.example;

import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.jdbc.AtomikosDataSourceBean;

import javax.transaction.UserTransaction;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Properties;

/**
 * 创 建 时 间: 2019/8/20
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class AtomikosJTATest {

    private static AtomikosDataSourceBean createAtomikosDataSourceBean(String dbName){
        Properties p = new Properties();
        p.setProperty("url","jdbc:mysql://192.168.3.17:3306/"+dbName);
        p.setProperty("user","root");
        p.setProperty("password","root");
        AtomikosDataSourceBean ds = new AtomikosDataSourceBean();
        ds.setXaProperties(p);
        ds.setUniqueResourceName(dbName);
        ds.setXaDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlXADataSource");
        return ds;
    }

    public static void main(String[] args) throws Exception {
        AtomikosDataSourceBean ds1 = createAtomikosDataSourceBean("example1");
        AtomikosDataSourceBean ds2 = createAtomikosDataSourceBean("example2");
        Connection conn1 = null;
        Connection conn2 = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        UserTransaction userTransaction = new UserTransactionImp();
        try{
            userTransaction.begin(); //开启事务

            conn1 = ds1.getConnection();
            ps1 = conn1.prepareStatement("INSERT INTO users (userName,`passWord`,user_sex,nick_name) VALUES ('example1','123456','man','example1')", Statement.RETURN_GENERATED_KEYS);
            ps1.execute();

            //模拟异常，2个都不会提交
            //int a = 1/0;

            conn2 = ds2.getConnection();
            ps2 = conn2.prepareStatement("INSERT INTO users (userName,`passWord`,user_sex,nick_name) VALUES ('example2','123456','man','example2')");
            ps2.execute();

            userTransaction.commit();
        }catch (Exception e){
            e.printStackTrace();
            userTransaction.rollback();
        }

    }
}
