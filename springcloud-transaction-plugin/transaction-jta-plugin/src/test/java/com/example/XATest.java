package com.example;

import com.mysql.jdbc.jdbc2.optional.MysqlXAConnection;
import com.mysql.jdbc.jdbc2.optional.MysqlXid;

import javax.sql.XAConnection;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * 创 建 时 间: 2019/8/20
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class XATest {

    public static void main(String[] args) throws Exception{
        boolean logXaCommands = true;//true:打印xa语句，用于调试
        //获得资源管理器操作接口实例 rm1
        Connection conn1 = DriverManager.getConnection("jdbc:mysql://192.168.3.17:3306/example1","root","root");
        XAConnection xaConn1 = new MysqlXAConnection((com.mysql.jdbc.Connection)conn1,logXaCommands);
        XAResource rm1 = xaConn1.getXAResource();
        //获得资源管理器操作接口实例 rm2
        Connection conn2 = DriverManager.getConnection("jdbc:mysql://192.168.3.17:3306/example2","root","root");
        XAConnection xaConn2 = new MysqlXAConnection((com.mysql.jdbc.Connection)conn2,logXaCommands);
        XAResource rm2 = xaConn2.getXAResource();
        //AP请求TM执行一个分布式事务，TM生成全局事务ID
        byte[] gtrid = "g12345".getBytes();
        int formatid = 1;
        try {
            //分别执行RM1和RM2上的事务分支
            //TM生成rm1上的事务分支ID
            byte[] bqual1 = "b00001".getBytes();
            Xid xid1 = new MysqlXid(gtrid,bqual1,formatid);
            //执行RM1上的事务分支
            rm1.start(xid1, XAResource.TMNOFLAGS);//one of tmnoflags ,gmjoin,
            PreparedStatement ps1 = conn1.prepareStatement("INSERT INTO users (userName,`passWord`,user_sex,nick_name) VALUES ('example1','123456','man','example1')");
            ps1.execute();
            rm1.end(xid1,XAResource.TMSUCCESS);

            //TM生成rm2上的事务分支ID
            byte[] bqual2 = "b00002".getBytes();
            Xid xid2 = new MysqlXid(gtrid,bqual2,formatid);
            //执行RM1上的事务分支
            rm2.start(xid2, XAResource.TMNOFLAGS);//one of tmnoflags ,gmjoin,
            PreparedStatement ps2 = conn2.prepareStatement("INSERT INTO users (userName,`passWord`,user_sex,nick_name) VALUES ('example2','123456','man','example2')");
            ps2.execute();
            rm2.end(xid2,XAResource.TMSUCCESS);

            //两阶段提交
            //phase1:询问所有的RM 准备提交事务分支
            int rm1_prepare = rm1.prepare(xid1);
            int rm2_prepare = rm2.prepare(xid2);
            //phase2:提交所有事务分支
            boolean onePhase = false;//TM判断有2个事务分支，所以不能优化为一段提交
            if(rm1_prepare==XAResource.XA_OK && rm2_prepare==XAResource.XA_OK){
                rm1.commit(xid1,onePhase);
                rm2.commit(xid2,onePhase);
            }else{//如果有事务分支没成功，则回滚
                rm1.rollback(xid1);
                rm2.rollback(xid2);
            }
        }catch (XAException e){
            e.printStackTrace();
        }









    }


}
