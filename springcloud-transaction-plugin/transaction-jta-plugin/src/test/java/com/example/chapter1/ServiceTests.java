package com.example.chapter1;

import com.example.TransactionJtaPluginApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.SpringApplication;

/**
 * 创 建 时 间: 2019/8/21
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TransactionJtaPluginApplication.class)
public class ServiceTests {

    @Autowired
    private TestService service;

    @Test
    public void parent(){

        service.parent();

    }

    @Test
    public void child(){

        service.child();

    }
}
