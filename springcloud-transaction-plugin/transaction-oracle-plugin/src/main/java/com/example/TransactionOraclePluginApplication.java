package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * http://localhost:8080/xxxx/druid/sql.html
 */
@SpringBootApplication
@EnableTransactionManagement
public class TransactionOraclePluginApplication {

	@Bean
	public DataSourceTransactionManager transactionManager(DataSource dataSource){

		return new DataSourceTransactionManager(dataSource);
	}

	public static void main(String[] args) {
		SpringApplication.run(TransactionOraclePluginApplication.class, args);
	}

}
