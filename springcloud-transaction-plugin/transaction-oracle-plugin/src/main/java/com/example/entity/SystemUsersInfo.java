package com.example.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 创 建 时 间: 2019/9/1
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Data
@Entity
@Table(name = "sys_user_info")
public class SystemUsersInfo implements Serializable {

    @Id
    private Integer id;
    @Column
    private String name;
    @Column
    private Integer age;
    @Column
    private Date createTime;

    public SystemUsersInfo() {
    }

    public SystemUsersInfo(Integer id, String name, Integer age, Date createTime) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.createTime = createTime;
    }
}
