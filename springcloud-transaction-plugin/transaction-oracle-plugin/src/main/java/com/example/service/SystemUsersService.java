package com.example.service;

import com.example.entity.SystemUsersInfo;
import com.example.mapper.SystemUsersInfoMapper;
import com.example.utils.thread.AsynTask;
import com.example.utils.thread.SpringThreadPool;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * 创 建 时 间: 2019/9/1
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
@Service
public class SystemUsersService {

    @Autowired
    private SystemUsersInfoMapper mapper;

    @Transactional
    public Integer save(SystemUsersInfo bean) {

        return mapper.save(bean);
    }

    @Transactional
    public void saves(List<SystemUsersInfo> beans) {
        long start = System.currentTimeMillis();
        ThreadPoolTaskExecutor threadPool = SpringThreadPool.getThreadPool();
        Future<Boolean> result = threadPool.submit(new AsynTask(beans));
        try {
            log.info(">>>>>>result:"+result.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        //mapper.saves(beans);
        long end = System.currentTimeMillis();
        log.info("花费："+(end-start)/1000.0+"秒");
    }
}
