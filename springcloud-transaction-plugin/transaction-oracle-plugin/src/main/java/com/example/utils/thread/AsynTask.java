package com.example.utils.thread;

import com.example.entity.SystemUsersInfo;
import com.example.mapper.SystemUsersInfoMapper;
import com.example.utils.SpringUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.catalina.core.ApplicationContext;
import org.springframework.aop.framework.AopContext;
import org.springframework.aop.support.AopUtils;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * 创 建 时 间: 2019/9/1
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@Log4j2
public class AsynTask implements Callable<Boolean> {

    private List<SystemUsersInfo> beans;
    @Override
    public Boolean call() throws Exception {
        long start = System.currentTimeMillis();
        log.info("异步执行保存操作开始 ... start:"+start);
        SystemUsersInfoMapper mapper = SpringUtil.getBean(SystemUsersInfoMapper.class);
        mapper.saves(beans);
        long end = System.currentTimeMillis();
        log.info("异步执行保存操作结束 ... end:"+end+" 花费："+(end-start)/1000.0+"秒");
        return true;
    }

    public AsynTask() {
    }

    public AsynTask(List<SystemUsersInfo> beans) {
        this.beans = beans;
    }

    //V get():获取结果，如果这个计算任务还没有执行结束，该调用线程会进入阻塞状态。如果计算任务已经被取消，
    // 调用get()会抛出CancellationException，如果计算过程中抛出异常，该方法会抛出ExecutionException，
    // 如果当前线程在阻塞等待的时候被中断了，该方法会抛出InterruptedException。


    public List<SystemUsersInfo> getBeans() {
        return beans;
    }

    public void setBeans(List<SystemUsersInfo> beans) {
        this.beans = beans;
    }
}
