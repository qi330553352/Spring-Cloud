package com.example.utils.thread;

import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 创 建 时 间: 2019/8/29
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
public class ThreadPool {

    private static ThreadPoolExecutor pool = null;

    private ThreadPool() {

    }

    public static ThreadPoolExecutor getInstance(){
        if(pool==null){
            pool = new ThreadPoolExecutor(//
                    4,//核心池的大小
                    20,//线程池最大线程数，它表示在线程池中最多能创建多少个线程
                    3,//表示线程没有任务执行时最多保持多久时间会终止
                    TimeUnit.SECONDS,
                    new ArrayBlockingQueue<Runnable>(20),// 一个阻塞队列，用来存储等待执行的任务
                    new CustomizableThreadFactory(),// 线程工厂，主要用来创建线程；
                    new ThreadPoolExecutor.AbortPolicy());//表示当拒绝处理任务时的策略，有以下四种取值：
            /*
            ThreadPoolExecutor.AbortPolicy:丢弃任务并抛出RejectedExecutionException异常。
            ThreadPoolExecutor.DiscardPolicy：也是丢弃任务，但是不抛出异常。
            ThreadPoolExecutor.DiscardOldestPolicy：丢弃队列最前面的任务，然后重新尝试执行任务（重复此过程）
            ThreadPoolExecutor.CallerRunsPolicy：只要线程池不关闭，该策略直接在调用者线程中，运行当前被丢弃的任务
            自定义拒绝策略，实现RejectedExecutionHandler接口
             */
        }
        return pool;
    }
    /*
    ArrayBlockingQueue ：一个由数组结构组成的有界阻塞队列。
    LinkedBlockingQueue ：一个由链表结构组成的有界阻塞队列。
    此队列的默认和最大长度为Integer.MAX_VALUE。此队列按照先进先出的原则对元素进行排序。
    PriorityBlockingQueue ：一个支持优先级排序的无界阻塞队列。
    DelayQueue：一个使用优先级队列实现的无界阻塞队列。
    SynchronousQueue：一个不存储元素的阻塞队列。
    LinkedTransferQueue：一个由链表结构组成的无界阻塞队列。
    LinkedBlockingDeque：一个由链表结构组成的双向阻塞队列。
     */
}
