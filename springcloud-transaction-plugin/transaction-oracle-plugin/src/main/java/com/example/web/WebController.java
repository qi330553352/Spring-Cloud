package com.example.web;

import com.example.entity.SystemUsersInfo;
import com.example.repository.SystemUsersRepository;
import com.example.service.SystemUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 创 建 时 间: 2019/9/1
 * 版       本: V1.0
 * 作       者: qixin
 * 版 权 所 有: 版权所有(C)2019-2029
 */
@RestController
public class WebController {

    @Autowired
    private SystemUsersService service;
    @Autowired
    private SystemUsersRepository repository;

    @GetMapping("test")
    public long test(){
        List<SystemUsersInfo> us = new ArrayList<>();
        for(int i=0;i<50;i++){
            SystemUsersInfo u = new SystemUsersInfo(i,"qixin"+i,i,new Date());

//            if(i==47){
//                u.setName("====================================================");
//            }
            us.add(u);
        }
        long start = System.currentTimeMillis();
        service.saves(us);
        //repository.saveAll(us);
        long end = System.currentTimeMillis();
        System.out.println("花费："+(end-start)/1000.0+"秒");

        return System.currentTimeMillis();
    }
}
