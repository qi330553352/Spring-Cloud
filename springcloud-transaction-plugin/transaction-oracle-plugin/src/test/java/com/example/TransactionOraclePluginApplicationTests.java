package com.example;

import com.example.entity.SystemUsersInfo;
import com.example.mapper.SystemUsersInfoMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionOraclePluginApplicationTests {

	@Autowired
	private SystemUsersInfoMapper mapper;

	@Test
	@Transactional
	public void contextLoads() {
		SystemUsersInfo usersInfo = new SystemUsersInfo();
		usersInfo.setId(11);
		usersInfo.setAge(1);
		usersInfo.setName("qixinsssss");
		usersInfo.setCreateTime(new Date());
		long start = System.currentTimeMillis();
		Integer u = mapper.save(usersInfo);
		long end = System.currentTimeMillis();
		System.out.println("花费："+(end-start)/1000.0+"秒");
		System.out.println("返回结果:"+u);
	}

}
