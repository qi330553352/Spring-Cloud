package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionRedisPluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionRedisPluginApplication.class, args);
	}

}
