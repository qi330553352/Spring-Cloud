package com.example;

import com.alibaba.fastjson.JSON;
import com.example.entity.User;
import com.example.service.RedisService;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionRedisPluginApplicationTests {

	@Autowired
	private RedisService service;

	@Test
	public void contextLoads() {
		//测试 Redis 的 string 类型
		service.setString("weichat","程序员私房菜");
		log.info("我的微信公众号为：{}", service.getString("weichat"));

		// 如果是个实体，我们可以使用 JSON 工具转成 JSON 字符串，
		User user = new User("CSDN", "123456");
		service.setString("userInfo", JSON.toJSONString(user));
		log.info("用户信息：{}", service.getString("userInfo"));
	}

}
